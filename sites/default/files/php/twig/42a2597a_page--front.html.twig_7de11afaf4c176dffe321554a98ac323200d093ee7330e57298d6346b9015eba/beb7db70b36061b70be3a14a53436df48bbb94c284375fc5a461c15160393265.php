<?php

/* themes/belconfect/page--front.html.twig */
class __TwigTemplate_fafbb73f066d67af44281783ed214aeef01dd559e4745d8f054238e9542182c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div id=\"debug\">
    <span class=\"visible-xs\">SIZE XS</span>
    <span class=\"visible-sm\">SIZE SM</span>
    <span class=\"visible-md\">SIZE MD</span>
    <span class=\"visible-lg\">SIZE LG</span>
</div> 
<style>
div#debug {
background-color: black;
color: white;
left: 0;
position: absolute;
top: 0;
}
</style>




<div class=\"container\">
    <div id=\"header_menu\" class=\"row\">
      <div class=\"col-xs-12\">
        ";
        // line 23
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "primary_menu", array()), "html", null, true));
        echo "              
      </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

    <div class=\"row\">
      <div class=\"col-xs-12\">
        <div class=\"schuine_rand\">
          <img src=\"img/logo.png\" alt=\"logo\" />
        </div><!-- /.schuine_rand -->
              
      </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->

  <div class=\"row\">
    <div class=\"col-md-4\">
      <div class=\"rode_schuine_rand\">
        <img src=\"img/schaar.jpg\" alt=\"schaar\" />
        <h3>teskt</h3> 
      </div><!-- /.rode_schuine_rand -->
    </div><!-- /.col-md-4 -->
    
    <div class=\"col-md-4\">
      <div class=\"groen_schuine_rand\">
        <img src=\"img/schaar.jpg\" alt=\"schaar\" />
        <h3>teskt</h3> 
      </div><!-- /.groen_schuine_rand -->
    </div><!-- /.col-md-4 -->
    
    <div class=\"col-md-4\">
      <div class=\"blauw_schuine_rand\">
        <img src=\"img/schaar.jpg\" alt=\"schaar\" />
        <h3>teskt</h3> 
      </div><!-- /.blauw_schuine_rand -->
    </div><!-- /.col-md-4 -->
  </div><!-- /.row -->

  <div class=\"row\">
    <main class=\"col-md-8\">
      main
      ";
        // line 62
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
      <h1>BETROUWBAAR, DUURZAAM  DYNAMISCH</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt nemo quaerat quos vero deleniti est id adipisci vitae nobis. Recusandae consectetur eius nulla, itaque error atque tempora, aspernatur consequatur sunt.</p>
    </main><!-- /.col-md-9 -->
    
    <aside class=\"col-md-4\">
    aside
    <ul class=\"facts\">
      <li>
        <p>Bel-Confect<br>bestaat 10 jaar</p>
        <span class=\"number\">10</span>
      </li>
      <li>
        <p>Bel-Confect<br>bestaat 10 jaar</p>
        <span class=\"number\">10</span>
      </li>
      <li>
        <p>Bel-Confect<br>bestaat 10 jaar</p>
        <span class=\"number\">10</span>
      </li>    
    </ul>

    </aside><!-- /.col-md-3 -->
  </div><!-- /.row -->


  <div class=\"row\">
    <div class=\"col-md-3\">
      ";
        // line 90
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "region_overons", array()), "html", null, true));
        echo "
    </div><!-- /.col-md-3 -->

    <div class=\"col-md-3\">
      ";
        // line 94
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "region_tech_info", array()), "html", null, true));
        echo "
    </div><!-- /.col-md-3 -->
    
    <div class=\"col-md-3\">
      ";
        // line 98
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "region_downloads", array()), "html", null, true));
        echo "
    </div><!-- /.col-md-3 -->
    
    <div class=\"col-md-3\">
      ";
        // line 102
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "region_contacteer", array()), "html", null, true));
        echo "
    </div><!-- /.col-md-3 -->
  </div><!-- /.row -->

  <div id=\"footer_top\" class=\"row\">
    <div class=\"col-md-3\">kolom1</div><!-- /.col-md-3 -->
    <div class=\"col-md-3\">kolom2</div><!-- /.col-md-3 -->
    <div class=\"col-md-3\">kolom3</div><!-- /.col-md-3 -->
    <div class=\"col-md-3\">kolom4</div><!-- /.col-md-3 -->
  </div><!-- /.row -->


  <footer class=\"row\">
    <div class=\"col-md-4\">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil tempora eius eveniet architecto vel praesentium. Dolore quos, officia natus, dicta iusto totam blanditiis assumenda necessitatibus tempora odio maxime, minus cum!</p>
    </div><!-- /.col-md-4 -->
    <div class=\"col-md-4\">2</div><!-- /.col-md-4 -->
    <div class=\"col-md-4\">3</div><!-- /.col-md-4 -->
  </footer><!-- /.row -->
</div><!-- /.container -->";
    }

    public function getTemplateName()
    {
        return "themes/belconfect/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 102,  154 => 98,  147 => 94,  140 => 90,  109 => 62,  67 => 23,  43 => 1,);
    }
}
/* <div id="debug">*/
/*     <span class="visible-xs">SIZE XS</span>*/
/*     <span class="visible-sm">SIZE SM</span>*/
/*     <span class="visible-md">SIZE MD</span>*/
/*     <span class="visible-lg">SIZE LG</span>*/
/* </div> */
/* <style>*/
/* div#debug {*/
/* background-color: black;*/
/* color: white;*/
/* left: 0;*/
/* position: absolute;*/
/* top: 0;*/
/* }*/
/* </style>*/
/* */
/* */
/* */
/* */
/* <div class="container">*/
/*     <div id="header_menu" class="row">*/
/*       <div class="col-xs-12">*/
/*         {{ page.primary_menu }}              */
/*       </div><!-- /.col-xs-12 -->*/
/*     </div><!-- /.row -->*/
/* */
/*     <div class="row">*/
/*       <div class="col-xs-12">*/
/*         <div class="schuine_rand">*/
/*           <img src="img/logo.png" alt="logo" />*/
/*         </div><!-- /.schuine_rand -->*/
/*               */
/*       </div><!-- /.col-xs-12 -->*/
/*     </div><!-- /.row -->*/
/* */
/*   <div class="row">*/
/*     <div class="col-md-4">*/
/*       <div class="rode_schuine_rand">*/
/*         <img src="img/schaar.jpg" alt="schaar" />*/
/*         <h3>teskt</h3> */
/*       </div><!-- /.rode_schuine_rand -->*/
/*     </div><!-- /.col-md-4 -->*/
/*     */
/*     <div class="col-md-4">*/
/*       <div class="groen_schuine_rand">*/
/*         <img src="img/schaar.jpg" alt="schaar" />*/
/*         <h3>teskt</h3> */
/*       </div><!-- /.groen_schuine_rand -->*/
/*     </div><!-- /.col-md-4 -->*/
/*     */
/*     <div class="col-md-4">*/
/*       <div class="blauw_schuine_rand">*/
/*         <img src="img/schaar.jpg" alt="schaar" />*/
/*         <h3>teskt</h3> */
/*       </div><!-- /.blauw_schuine_rand -->*/
/*     </div><!-- /.col-md-4 -->*/
/*   </div><!-- /.row -->*/
/* */
/*   <div class="row">*/
/*     <main class="col-md-8">*/
/*       main*/
/*       {{ page.content }}*/
/*       <h1>BETROUWBAAR, DUURZAAM  DYNAMISCH</h1>*/
/*       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt nemo quaerat quos vero deleniti est id adipisci vitae nobis. Recusandae consectetur eius nulla, itaque error atque tempora, aspernatur consequatur sunt.</p>*/
/*     </main><!-- /.col-md-9 -->*/
/*     */
/*     <aside class="col-md-4">*/
/*     aside*/
/*     <ul class="facts">*/
/*       <li>*/
/*         <p>Bel-Confect<br>bestaat 10 jaar</p>*/
/*         <span class="number">10</span>*/
/*       </li>*/
/*       <li>*/
/*         <p>Bel-Confect<br>bestaat 10 jaar</p>*/
/*         <span class="number">10</span>*/
/*       </li>*/
/*       <li>*/
/*         <p>Bel-Confect<br>bestaat 10 jaar</p>*/
/*         <span class="number">10</span>*/
/*       </li>    */
/*     </ul>*/
/* */
/*     </aside><!-- /.col-md-3 -->*/
/*   </div><!-- /.row -->*/
/* */
/* */
/*   <div class="row">*/
/*     <div class="col-md-3">*/
/*       {{ page.region_overons }}*/
/*     </div><!-- /.col-md-3 -->*/
/* */
/*     <div class="col-md-3">*/
/*       {{ page.region_tech_info }}*/
/*     </div><!-- /.col-md-3 -->*/
/*     */
/*     <div class="col-md-3">*/
/*       {{ page.region_downloads }}*/
/*     </div><!-- /.col-md-3 -->*/
/*     */
/*     <div class="col-md-3">*/
/*       {{ page.region_contacteer }}*/
/*     </div><!-- /.col-md-3 -->*/
/*   </div><!-- /.row -->*/
/* */
/*   <div id="footer_top" class="row">*/
/*     <div class="col-md-3">kolom1</div><!-- /.col-md-3 -->*/
/*     <div class="col-md-3">kolom2</div><!-- /.col-md-3 -->*/
/*     <div class="col-md-3">kolom3</div><!-- /.col-md-3 -->*/
/*     <div class="col-md-3">kolom4</div><!-- /.col-md-3 -->*/
/*   </div><!-- /.row -->*/
/* */
/* */
/*   <footer class="row">*/
/*     <div class="col-md-4">*/
/*       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil tempora eius eveniet architecto vel praesentium. Dolore quos, officia natus, dicta iusto totam blanditiis assumenda necessitatibus tempora odio maxime, minus cum!</p>*/
/*     </div><!-- /.col-md-4 -->*/
/*     <div class="col-md-4">2</div><!-- /.col-md-4 -->*/
/*     <div class="col-md-4">3</div><!-- /.col-md-4 -->*/
/*   </footer><!-- /.row -->*/
/* </div><!-- /.container -->*/
